#== Sets up clear console command ==#
from os import system, name

def clear():
    # If windows
    if name == 'nt':
        _ = system('cls')
    # for mac/linux
    else:
        _ = system('clear')

if name == 'nt':
    # Activates the use of color in the terminal for windows
    import ctypes
    
    kernel32 = ctypes.windll.kernel32
    kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)
    
#=====================================================#
#== Imports ==#
from enum import Enum, unique

import math
import sys
import datetime

#== End Imports ==#

#== Global ==#
dijkstra = False
bfs = False

#== Wrapper Enum for colors ==#
@unique
class Color(Enum):
    Clear = "\033[0m"
    Start = "\033[48;5;1m"
    Stop = "\033[48;5;4m"
    Forest = "\033[48;5;34m"
    Grass = "\033[48;5;82m"
    Mountain = "\033[48;5;240m"
    Water = "\033[48;5;33m"
    Road = "\033[48;5;94m"
    Path = "\033[48;5;93m"
    TextRed = "\033[38;5;9m"
    TextBlue = "\033[38;5;12m"
#== End Color ==#

#== Function definitions ==#
# Print function for the map with color values and text based on selected map
def printMap(grid):
    print("")
    for x in range(0, grid.sizex):
        for y in range(0, grid.sizey):
            sys.stdout.write(grid.tiles[x][y].color + grid.tiles[x][y].character + Color.Clear.value)
        print("")
    print("")
    
# Heuristic cost function
def heuristic(tile, finish):
    dx = abs(tile.posx - finish.posx)
    dy = abs(tile.posy - finish.posy)
    return math.sqrt((dx * dx) + (dy * dy))
    
# Print function for menu
def printMenu():
    if dijkstra:
        print("\nAlgorithm: Dijkstra\n")
    elif bfs:
        print("\nBFS\n")
    else:
        print("\nAlgorithm: A*\n")
    print("Main menu")
    print("A - A*")
    print("B - BFS")
    print("D - Dijkstra's")
    print("1 - Map 1-1")
    print("2 - Map 1-2")
    print("3 - Map 1-3")
    print("4 - Map 1-4")
    print("5 - Map 2-1")
    print("6 - Map 2-2")
    print("7 - Map 2-3")
    print("8 - Map 2-4")
    print("Q - Quit\n")
    return input("Choice: ")
#== End function def ==#

#== Tile class for the grid ==#
class Tile:
    def __init__(self, posx=0, posy=0, character='', color=Color.Clear.value, cost=1, travers=True):
        self.posx = posx
        self.posy = posy
        self.neighbor = list()
        self.f = 1000000.0
        self.g = 1000000.0
        self.h = 0.0
        self.cost = cost
        self.traversable = travers
        self.color = color
        self.character = character
        self.parent = None
    # End def __init__
 
# Predefine class function
def addNeighbor(self, tile=Tile()):
    self.neighbor.append(tile)

# Add class function to class
Tile.addNeighbor = addNeighbor
#== End Tile class ==#

#== Grid class ==#
class Grid:
    def __init__(self, file):
        self.tiles = list()
        self.sizex = 0
        self.sixey = 0
        self.path = None

        for line in file:
            self.tiles.append([])
            y = 0

            for c in line:
                tempTravers = True
                tempCost = 1
                tempColor = Color.Clear.value

                if (c == "A"):
                    tempColor = Color.Start.value
                elif (c == "B"):
                    tempColor = Color.Stop.value
                elif (c == "#"):
                    tempTravers = False
                    tempColor = Color.Mountain.value
                elif (c == "w"):
                    tempColor = Color.Water.value
                    tempCost = 100
                elif (c == "m"):
                    tempColor = Color.Mountain.value
                    tempCost = 50
                elif (c == "f"):
                    tempColor = Color.Forest.value
                    tempCost = 10
                elif (c == "g"):
                    tempColor = Color.Grass.value
                    tempCost = 5
                elif (c == "r"):
                    tempColor = Color.Road.value
                    tempCost = 1
                elif (c == "\n"):
                    break
                self.tiles[self.sizex].append(Tile(self.sizex, y, c, tempColor, tempCost, tempTravers))
                y += 1

            self.sizex += 1
            self.sizey = y
    # end def __init_
    
    def findNeighbor(self, tile):
        if tile.posx >= 1:
            tile.addNeighbor(self.tiles[tile.posx - 1][tile.posy]) # Left
        if tile.posy >= 1:
            tile.addNeighbor(self.tiles[tile.posx][tile.posy - 1]) # Up
        if tile.posx < self.sizex - 1:
            tile.addNeighbor(self.tiles[tile.posx + 1][tile.posy]) #Right
        if tile.posy < self.sizey - 1:
            tile.addNeighbor(self.tiles[tile.posx][tile.posy + 1]) # Down
    # End def findNeighbor

    def pathfinding(self):
        openList = list()
        closedList = list()

        self.path = list()

        totalCost = 0
        
        finish = Tile()
        for row in self.tiles:
            for tile in row:
                if tile.character == "A":
                    openList.append(tile)
                if tile.character == "B":
                    finish = tile
        
        t2 = None
        t1 = datetime.datetime.now()
        if not dijkstra:
            openList[0].h = heuristic(openList[0], finish)
            
        openList[0].g = 0

        while openList:
            index = 0
            i = 0
            tempCost = 10000000
            if not bfs:
                for tile in openList:
                    if tile.f <= tempCost:
                        tempCost = tile.f
                        index = i
                    i += 1

            currentTile = openList.pop(index)

            if currentTile != finish:
                closedList.append(currentTile)
                self.findNeighbor(currentTile)
                
                for tile in currentTile.neighbor:
                    if tile not in set(closedList) and tile.traversable:
                        if bfs: 
                            if tile not in set(openList):
                                openList.append(tile)
                                tile.parent = currentTile
                        else:
                            tempg = currentTile.g + tile.cost

                            if tile not in set(openList):
                                    openList.append(tile)
                            
                            if tempg >= tile.g:
                                continue

                            tile.g = tempg
                            tile.parent = currentTile

                            if not dijkstra:
                                tile.h = heuristic(tile, finish)

                            tile.f = tile.g + tile.h

            elif currentTile == finish:
                t2 = datetime.datetime.now()
                self.path.append(currentTile)
                while currentTile.parent:
                    self.path.append(currentTile.parent)
                    currentTile = currentTile.parent
                for tile in openList:
                    tile.color += Color.TextBlue.value
                for tile in closedList:
                    tile.color += Color.TextRed.value
                for tile in self.path:
                    tile.color = Color.Path.value
                    totalCost += tile.cost
                openList.clear()
                closedList.clear()

        printMap(self)
        print("Total cost: ", totalCost)
        time = abs(t1 - t2)
        print("Time: ", round(time.total_seconds() * 1000, 3), "ms")


    # End def pathfinding
#== End Grid class==#

#== Main loop ==#
userInput = ""

while (userInput != "Q" and userInput != "q"):
    userInput = printMenu()
    clear()
    if userInput == '1':
        grid = Grid(open("boards/board-1-1.txt", "r"))
        grid.pathfinding()
    if userInput == '2':
        grid = Grid(open("boards/board-1-2.txt", "r"))
        grid.pathfinding()
    if userInput == '3':
        grid = Grid(open("boards/board-1-3.txt", "r"))
        grid.pathfinding()
    if userInput == '4':
        grid = Grid(open("boards/board-1-4.txt", "r"))
        grid.pathfinding()
    if userInput == '5':
        grid = Grid(open("boards/board-2-1.txt", "r"))
        grid.pathfinding()
    if userInput == '6':
        grid = Grid(open("boards/board-2-2.txt", "r"))
        grid.pathfinding()
    if userInput == '7':
        grid = Grid(open("boards/board-2-3.txt", "r"))
        grid.pathfinding()
    if userInput == '8':
        grid = Grid(open("boards/board-2-4.txt", "r"))
        grid.pathfinding()
    if userInput == 'd' or userInput == 'D':
        dijkstra = True
        bfs = False
    if userInput == 'a' or userInput == 'A':
        dijkstra = False
        bfs = False
    if userInput == 'b' or userInput == 'B':
        bfs = True
        dijkstra = False
